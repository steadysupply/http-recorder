package main

import (
    "bufio"
    "bytes"
    "crypto/sha256"
    "encoding/hex"
    "encoding/json"
    "fmt"
    "io"
    "io/ioutil"
    "log"
    "net"
    "net/http"
    "net/http/httputil"
    "net/url"
    "os"
    "path"
)

var client http.Client

type RequestJSON struct {
    Method              string
    URL                 *url.URL
    Header              http.Header
    Body                []byte
    ContentLength       int64
    TransferEncoding    []string
    RequestURI          string
}

type ResponseJSON struct {
    Status              string
    StatusCode          int
    Header              http.Header
    Body                []byte
    ContentLength       int64
    TransferEncoding    []string
    Uncompressed        bool
}

type Pair struct {
    Request     *RequestJSON
    Response    *ResponseJSON
}

func marshal_pair(
    request     *http.Request,
    response    *http.Response,
) ([]byte, error) {
    // marshal a request/response pair

    request_body, err := ioutil.ReadAll(request.Body);
    if err != nil { log.Fatal(err) }


    request_json := RequestJSON{
        Method:             response.Request.Method,
        URL:                response.Request.URL,
        Header:             response.Request.Header,
        Body:               request_body,
        ContentLength:      response.Request.ContentLength,
        TransferEncoding:   response.Request.TransferEncoding,
        RequestURI:         response.Request.RequestURI,
    }

    response_body, err := ioutil.ReadAll(response.Body);
    if err != nil { log.Fatal(err) }

    response_json := ResponseJSON{
        Status:             response.Status,
        StatusCode:         response.StatusCode,
        Header:             response.Header,
        Body:               response_body,
        ContentLength:      response.ContentLength,
        TransferEncoding:   response.TransferEncoding,
        Uncompressed:       response.Uncompressed,
    }

    pair_json, err := json.MarshalIndent(
        &Pair{&request_json, &response_json}, "", "  ",
    )
    if err != nil { log.Fatal(err) }
    return pair_json, nil
}

func recording_proxy(
    target_host *string,
    workdir *string, http_connection net.Conn,
) error {
    // proxy requests to `target_host`,
    // write the request/response pair to disk

    defer http_connection.Close()

    request_in, err := http.ReadRequest(bufio.NewReader(http_connection))
    if err != nil { log.Fatal(err) }

    // read incoming request body to []byte and make a reader out of it
    in_bytes, err := ioutil.ReadAll(request_in.Body);
    request_in.Body.Close()
    if err != nil { log.Fatal(err) }
    // the below implements io.Seeker
    in_reader := bytes.NewReader(in_bytes)

    // modify request to point at target
    request_in.URL.Scheme = "http"
    request_in.URL.Host = *target_host

    // construct outbound request
    request_out, err := http.NewRequest(
        request_in.Method,
        request_in.URL.String(),
        ioutil.NopCloser(in_reader),
    )
    if err != nil { log.Fatal(err) }

    // copy headers over
    for name := range request_in.Header {
        request_out.Header.Set(name, request_in.Header.Get(name))
    }

    // send modified request on to target
    response, err := client.Do(request_out)
    if err != nil { log.Fatal(err) }

    // read response body to []byte
    out_bytes, err := ioutil.ReadAll(response.Body);
    response.Body.Close()
    if err != nil { log.Fatal(err) }
    out_reader := bytes.NewReader(out_bytes)

    // render to struct, io.Seeker interface comes in handy
    in_reader.Seek(0, io.SeekStart)
    request_in.Body = ioutil.NopCloser(in_reader)
    response.Body = ioutil.NopCloser(out_reader)
    pair, err := marshal_pair(request_in, response)
    if err != nil { log.Fatal(err) }

    // create an identifier on (method, path, body)
    hash := sha256.New()
    hash.Write([]byte(request_in.Method))
    hash.Write([]byte(request_in.RequestURI))
    hash.Write(in_bytes)
    name := hex.EncodeToString(hash.Sum(nil))
    log.Printf("%s %s -> %s", request_in.Method, request_in.RequestURI, name)

    // dump the pair as json
    write_path := path.Join(*workdir, fmt.Sprintf("%s.json", name))
    err = ioutil.WriteFile(write_path, pair, 0664)
    if err != nil { log.Fatal(err) }


    // pass the response on to the requester
    out_reader.Seek(0, io.SeekStart)
    response.Body = ioutil.NopCloser(out_reader)
    response_string, err := httputil.DumpResponse(response, true)
    if err != nil { log.Fatal(err) }

    _, err = http_connection.Write(response_string)
    if err != nil { log.Fatal(err) }

    return nil
}

func main() {
    target_host, ok := os.LookupEnv("TARGET_HOST")
                 if !ok { panic("set TARGET_HOST") }
    listen, ok := os.LookupEnv("LISTEN")
            if !ok { panic("set LISTEN") }
    workdir, ok := os.LookupEnv("WORKDIR")
             if !ok { panic("set WORKDIR") }

    log.SetFlags(log.Ltime | log.Llongfile)

    server, server_err := net.Listen("tcp", listen)
    log.Printf("%s -> %s (%s)", listen, target_host, workdir)
    if server_err != nil { log.Fatal(server_err) }

    for {
        http_connection, err := server.Accept()
        if err != nil { log.Fatal(err) }
        go recording_proxy(&target_host, &workdir, http_connection)
    }
}
