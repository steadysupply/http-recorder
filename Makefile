.PHONY: test
DOCKER_COMPOSE=docker-compose --project-directory .
IMAGE_NAME=http-recorder
GIT_SHA=$(shell git rev-parse --short HEAD)

define DOCKER_COMPOSE_DEVELOP
	$(DOCKER_COMPOSE) \
		-f docker/develop/compose.yml
endef

define DOCKER_COMPOSE_TEST
	$(DOCKER_COMPOSE) \
		-f docker/develop/compose.yml \
		-f docker/develop/test/compose.yml
endef

define DOCKER_COMPOSE_CI_TEST
	$(DOCKER_COMPOSE) \
		-f docker/develop/compose.yml \
		-f docker/develop/test/compose.yml \
		-f ci/compose.yml
endef


up-daemon:
	$(DOCKER_COMPOSE_DEVELOP) build
	$(DOCKER_COMPOSE_DEVELOP) up -d --force-recreate

develop: up-daemon
	$(DOCKER_COMPOSE_DEVELOP) exec mitm ash

logs:
	$(DOCKER_COMPOSE_DEVELOP) logs

request:
	$(DOCKER_COMPOSE_DEVELOP) exec requester http mitm/post a=b c=d

down:
	$(DOCKER_COMPOSE_DEVELOP) down

test:
	$(DOCKER_COMPOSE_TEST) build
	$(DOCKER_COMPOSE_TEST) run --rm requester pytest -s

build:
	docker build -t $(IMAGE_NAME) --target release .

push: build
	docker tag $(IMAGE_NAME) steadysupply/$(IMAGE_NAME):$(GIT_SHA)
	docker tag $(IMAGE_NAME) steadysupply/$(IMAGE_NAME):latest
	docker push steadysupply/$(IMAGE_NAME):$(GIT_SHA)
	docker push steadysupply/$(IMAGE_NAME):latest

ci-test:
	$(DOCKER_COMPOSE_CI_TEST) pull -q
	$(DOCKER_COMPOSE_CI_TEST) build
	$(DOCKER_COMPOSE_CI_TEST) push
	$(DOCKER_COMPOSE_CI_TEST) run --rm requester \
		pytest --junitxml=pytest.xml
