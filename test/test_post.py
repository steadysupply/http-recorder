import hashlib
import json

import requests


def expected_name(method, uri, body):
    hash = hashlib.sha256()
    hash.update(method)
    hash.update(uri)
    hash.update(body)
    return "{0}.json".format(hash.hexdigest())


def test_basic():
    method = b'POST'
    body = b'{"a": "b"}'
    uri = b'/post'
    url = "http://mitm{0}".format(uri.decode('utf8'))

    # we expect httpbin to send our body back to us (this asserts the mitm
    # doesn’t fudge our response)
    response = requests.request(method, url, data=body)
    assert response.json()['json'] == json.loads(body.decode('utf8'))

    # we also expect a file to be written containing our body (this asserts a 
    # record of our request and its response was made)
    record_path = "/test/out/{0}".format(expected_name(method, uri, body))
    with open(record_path, 'r') as fh:
        record = json.load(fh)
        assert record['Request']
        assert record['Response']
